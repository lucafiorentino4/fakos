// JAVASCRIPT FÜR DIE SEITE FAKOS ###############################################


  var loginButton = document.querySelector("#loginButton");
  var kontaktButton = document.querySelector("#kontaktButton");
  var kontaktLayerButtonNavi = document.querySelector("#kontaktLayerButtonNavi");
  var kontaktLayerButtonNaviDesktop = document.querySelector("#kontaktLayerButtonNaviDesktop");
  var loginButtonDesktop = document.querySelector("#loginButtonDesktop");

  loginButton.addEventListener("click", function() { openLayer('loginLayer'); });
  kontaktButton.addEventListener("click", function() { openLayer('kontaktLayer'); });
  kontaktLayerButtonNavi.addEventListener("click", function() { openLayer('kontaktLayer'); });
  kontaktLayerButtonNaviDesktop.addEventListener("click", function() { openLayer('kontaktLayer'); });
  loginButtonDesktop.addEventListener("click", function() { openLayer('loginLayer'); });

  var loginClose = document.querySelector("#loginClose");
  var kontaktClose = document.querySelector("#kontaktClose");

  loginClose.addEventListener("click", function() { closeLayer('loginLayer'); });
  kontaktClose.addEventListener("click", function() { closeLayer('kontaktLayer'); });


  function openLayer(layerName) {

    var formularLayer = document.getElementById(layerName);

    formularLayer.style.display = "block";

  }

  function closeLayer(layerName) {

    var formularLayer = document.getElementById(layerName);

    formularLayer.style.display = "none";
  }




// LAYER JS FOR POST LAYER
  var postGreeceButton = document.querySelector("#postGreeceButton");
  var postItalyButton = document.querySelector("#postItalyButton");
  var postNorwayButton = document.querySelector("#postNorwayButton");
  var postChinaButton = document.querySelector("#postChinaButton");
  var postFranceButton = document.querySelector("#postFranceButton");
  var postGeorgiaButton = document.querySelector("#postGeorgiaButton");
//----------------------------------------------------------------------------
// LAYER JS FOR POST LAYER
  postGreeceButton.addEventListener("click", function() { openLayer('postGreeceLayer')});
  postItalyButton.addEventListener("click", function() { openLayer('postItalyLayer')});
  postNorwayButton.addEventListener("click", function() { openLayer('postNorwayLayer')});
  postChinaButton.addEventListener("click", function() { openLayer('postChinaLayer')});
  postFranceButton.addEventListener("click", function() { openLayer('postFranceLayer')});
  postGeorgiaButton.addEventListener("click", function() { openLayer('postGeorgiaLayer')});
//----------------------------------------------------------------------------


// LAYER JS FOR POST LAYER
  var postLayerGreeceClose = document.querySelector("#postCloseGreece");
  var postLayerItalyClose = document.querySelector("#postCloseItaly");
  var postLayerNorwayClose = document.querySelector("#postCloseNorway");
  var postLayerChinaClose = document.querySelector("#postCloseChina");
  var postLayerFranceClose = document.querySelector("#postCloseFrance");
  var postLayerGeorgiaClose = document.querySelector("#postCloseGeorgia");
//----------------------------------------------------------------------------
// LAYER JS FOR POST LAYER
  postLayerGreeceClose.addEventListener("click", function() { closeLayer('postGreeceLayer'); });
  postLayerItalyClose.addEventListener("click", function() { closeLayer('postItalyLayer'); });
  postLayerNorwayClose.addEventListener("click", function() { closeLayer('postNorwayLayer'); });
  postLayerChinaClose.addEventListener("click", function() { closeLayer('postChinaLayer'); });
  postLayerFranceClose.addEventListener("click", function() { closeLayer('postFranceLayer'); });
  postLayerGeorgiaClose.addEventListener("click", function() { closeLayer('postGeorgiaLayer'); });
//----------------------------------------------------------------------------






//----------------- Slider --------------------

var heroSlider = document.querySelectorAll('.heroslider');
var heroSliderContainer = document.querySelector('.hero-slider-container');
var prevButton = document.querySelector('.prev-slide');
var nextButton = document.querySelector('.next-slide');

var activeSlide = 0;

console.log(heroSlider);

prevButton.addEventListener('click', function(){
  showSlider(activeSlide -1)
});
nextButton.addEventListener('click', function(){
  showSlider(activeSlide +1)
});

function hideHeroSlider(){
  for(var i = 0; i < heroSlider.length; i++){
    heroSlider[i].style.display = 'none';
  }
}

function showSlider(x) {
  hideHeroSlider();

  if (x >= heroSlider.length) x = 0;
  if (x < 0) x = heroSlider.length-1;

  activeSlide = x;
  heroSlider[x].style.display = 'flex';

}

showSlider(activeSlide);
